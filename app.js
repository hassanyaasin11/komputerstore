//All elements from html are provided here.
const computersElement = document.getElementById("computers");
const titleElement = document.getElementById("title");
const priceElement = document.getElementById("price");
const descriptionElement = document.getElementById("description")
const imageElement = document.getElementById("image");
const specsElement = document.getElementById("specs");
const loanElement = document.getElementById("loan");
const bankElement = document.getElementById("bank");
const outstandingLoanElement = document.getElementById("outstandingLoan")
const workElement = document.getElementById("work");
const payElement = document.getElementById("pay");
const balanceElement = document.getElementById("balance");
const repayButtonElement = document.getElementById("repayButton");
const buyNowElement = document.getElementById("buyNow");


//Global variables are declared here, these are used in the functions.
let selectedcomputer;
let computers=[];
let bankBalance = 0.0;
let salary=0.0;
let loanCounter=0;
let outstandingLoan = 0.0;
let deduction = 0.0;

//Fetching data from API
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
.then(response => response.json())
.then(data => computers=data)
.then(computers => startPage(computers));


//This function saves the computers in an array.
const addcomputersToInfo = (computers) => {
    computers.forEach(x => addcomputerToInfo(x));
}

//This function saves attributes of each computer.
const addcomputerToInfo = (computer) =>{
    const computerElement = document.createElement("option");
    computerElement.value = computer.id;
    computerElement.appendChild(document.createTextNode(computer.title));
    computersElement.appendChild(computerElement);
}

//This function sets the default layout of the website, this happens everytime the website refreshes or when it is accessed for the first time.
const startPage =(computers) =>{
    addcomputersToInfo(computers)

    selectedcomputer = computers[0];
    titleElement.innerText = selectedcomputer.title;
    imageElement.src = `https://noroff-komputer-store-api.herokuapp.com/${selectedcomputer.image}`;
    imageElement.width = "200";
    imageElement.height = "200";
    imageElement.style.float="center"
    priceElement.innerText = `${selectedcomputer.price.toFixed(2)} Kr`;
    descriptionElement.innerText = selectedcomputer.specs;
    specsElement.innerText = selectedcomputer.description;
}

//This function changes the "Info" section of the website, shows information of each computer.
const handlecomputerInfoChange = e =>{
    selectedcomputer = computers[e.target.selectedIndex];
    titleElement.innerText = selectedcomputer.title;
    imageElement.src = `https://noroff-komputer-store-api.herokuapp.com/${selectedcomputer.image}`;
    imageElement.width = "200";
    imageElement.height = "200";
    imageElement.style.float="center"
    priceElement.innerText = `${selectedcomputer.price.toFixed(2)} Kr`;
    descriptionElement.innerText = selectedcomputer.specs;
    specsElement.innerText = selectedcomputer.description;
}

//This function allows the user to take loan, loan greater than half of bank balance cannot be granted. 
//Nor can the user take out more than one loan.
let loanFunction = function(){

    if(outstandingLoan<=0)
    loanCounter=0;

    const moneyToLoan=prompt("How much money?");

    //More than one loan cannot be granted.
    if(loanCounter!=0)
        alert("You are in debt, repay your current loan!");

    else if (moneyToLoan != null && moneyToLoan <= (bankBalance*2)) {
        outstandingLoan = outstandingLoan + parseFloat(moneyToLoan)
        bankBalance+= outstandingLoan
        loanCounter++

        outstandingLoanElement.innerText = `Outstanding loan: ${parseFloat(outstandingLoan).toFixed(2)} Kr`;
        balanceElement.innerText = `Balance: ${parseFloat(bankBalance).toFixed(2)} Kr`;

        if (repayButtonElement.style.display === "none")
        repayButtonElement.style.display = "block";

        if(outstandingLoan!=0)
        outstandingLoanElement.style.display="block"
    }

    //When the user cancels the "Prompt" popup box without writing down loan, nothing should happen.
    else if(!moneyToLoan){
        return
    }

    else
    alert("Try again");
}

//This function is the "Bank" button, it transfers the "Pay balance" to the "Bank balance".
let transferSalaryFunction = function(){

    if(salary!=0){
        deduction=(salary*0.1)
        if(outstandingLoan!=0){
            salary-=deduction
            outstandingLoan-=deduction
            
            //To avoid "negative" loan which could lead to miscalculations
            if(outstandingLoan < deduction){
                let rest = outstandingLoan*-1;
                outstandingLoan += rest; 
                salary+= rest
            }

            //When the debt is repayed
            if(salary<outstandingLoan)
            bankBalance-=outstandingLoan

            bankBalance+=salary
            salary-=salary
        }

        else{
            bankBalance+=salary
            salary-=salary
        }

        balanceElement.innerText= `Balance: ${parseFloat(bankBalance).toFixed(2)} Kr`;
        payElement.innerText = `Pay: ${salary.toFixed(2)} Kr`;

        if(outstandingLoan!=0){
            outstandingLoanElement.innerText = `Outstanding loan: ${parseFloat(outstandingLoan).toFixed(2)} Kr`;
            outstandingLoanElement.style.display="block"
        }

        if(outstandingLoan<=0){
            repayButtonElement.style.display = "none"
            outstandingLoanElement.style.display="none"
            loanCounter=0;
        }
    }
    else
        alert('You have no salary!')
}

//This functions is the "Work" button, it increases the "Pay balance" with 100 Kr.
let workFunction = ()=>{
    salary+=100;
    payElement.innerText = `Pay: ${salary.toFixed(2)} Kr`;
}

//This function allows the user to repay the loan, the function will be executed by clicking on the "Repay loan" button.
let repayButtonFunction = ()=>{
    
    if(outstandingLoan<=salary){
        salary-=outstandingLoan
        outstandingLoan-=outstandingLoan
        bankBalance+=salary
        salary-=salary
    }
    else{
        outstandingLoan-=salary
        salary-=salary
    }
    outstandingLoanElement.innerText = `Outstanding loan: ${parseFloat(outstandingLoan).toFixed(2)} Kr`;
    if(outstandingLoan<=0){
        repayButtonElement.style.display = "none"
        outstandingLoanElement.style.display="none"
    }
    balanceElement.innerText= `Balance: ${parseFloat(bankBalance).toFixed(2)} Kr`;
    payElement.innerText = `Pay: ${salary.toFixed(2)} Kr`;
}

//This function allows the user to buy the selected computer, it can be executed by clicking on the "BUY NOW" button.
let buyNowFunction = () =>{
   if(bankBalance >= selectedcomputer.price){
    bankBalance-=selectedcomputer.price;
    alert("You have successfully purchased a computer!")
    balanceElement.innerText= `Balance: ${parseFloat(bankBalance).toFixed(2)} Kr`;
   }
   else
    alert("You cannot afford the price")
}

//All the event listeners are listed here, the elements will invoke the functions from here.
computersElement.addEventListener("change", handlecomputerInfoChange);
loanElement.addEventListener("click", loanFunction);
bankElement.addEventListener("click", transferSalaryFunction);
workElement.addEventListener("click", workFunction)
buyNowElement.addEventListener("click", buyNowFunction)
repayButtonElement.addEventListener("click", repayButtonFunction)
window.addEventListener("load", startPage)